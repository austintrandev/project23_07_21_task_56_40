package com.devcamp.hellopizzalight.api;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Random;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CPizzaCampaign {
@CrossOrigin
@GetMapping("/pizza-welcome")
public String getDateViet() {
	String name = "pizza lover";
	DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
	LocalDate today = LocalDate.now(ZoneId.systemDefault());
	return String.format("Hello %s ! Hôm nay %s, mua 1 tặng 1. ",name,  dtfVietnam.format(today));
}

@CrossOrigin
@GetMapping("/lucky-random-number")
public String luckyRandomNumber() {
	Random r = new Random();
	String userName = "Chanhtt";
	String fistName = "Austin";
	String lastName = "Tran";
	int randomIntNum = r.nextInt((6 - 1) + 1) + 1;
	return String.format("Xin chào %s, Số may mắn hôm nay của bạn là: %s ",userName,randomIntNum );
}
}
 